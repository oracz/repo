import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import oracle.kv.Direction;
import oracle.kv.KVStore;
import oracle.kv.Key;
import oracle.kv.loader.AbstractLoader;
import oracle.kv.loaderFactory.JsonLoaderFactory;
import oracle.kv.loaderFactory.LoaderFactory;
import oracle.kv.loaderFactory.StringLoaderFactory;
import oracle.kv.loaderFactory.XMLLoaderFactory;
import oracle.kv.utils.FileUtil;
import oracle.kv.utils.StringUtil;
import oracle.kv.utils.logger;
import oracle.kv.KVStoreConfig;
import oracle.kv.KVStoreFactory;


public class LoaderTest {
	

    static String fileType= "nothing";
    static String inputPathStr;
    static boolean load;
    static String storeName = "kvstore";
    static String hostName = "localhost";
    static String hostPort = "5000";

    static KVStore kvStore;

    static public String delimiter = ",";
    static public String minorArgs;
    static public String majorArgs;
    static public String valueArgs;

   static boolean cleanKVStore = false;
   static boolean printKVStore = false;
	
    public void usage(String message) {
        System.out.println("\n" +
                message + "\n");
        System.out.println("usage: " + getClass().getName());
        System.out.println("\t-s <store name> (default: kvstore) " +
                           "\n\t-h <host name> (default: localhost) " +
                           "\n\t-p <port number> (default: 5000)" +
                           "\n\t-i <input directory> (This is where your data is stored) " +
                           "\n\t-d <delimiter used> (default: ',') " +
                           "\n\t-m <major key component/s> (example: -m \"US 1 2\" for '-t=csv' or -m=\"US <tagName1> <tagName2>\" for '-t=xml') " +
                           "\n\t-n <minor key component/s> (example: -n \"info 3\" for '-t=csv' or -n=\"info <tagName3>\" for '-t=xml') " +
                           "\n\t-v <value> (example: -v \"1 2 3 \" for '-t=csv' or -v=\"<tagName1> <tagName2> <tagName3>\" for '-t=xml' ) " +
                           "\n\t-t <input file type xml/csv> (default: csv) " +
                           "\n\t-c <clear kv-store true/false> (default: false)  " +
                           "\n\t-k (display keys in the store)  " +
                           "\n\t-u (Shows the usage)");
        System.exit(0);
    }
    
    /**
     * Parses command line args and opens the KVStore.
     */
    public void parseCommandLineArgs(String[] args){
        final int nArgs = args.length;
        int argc = 0;

        if (nArgs < 1) {
            usage("");
        }


        while (argc < nArgs) {
            final String thisArg = args[argc++];

            if (thisArg.equals("-i")) {
                if (argc < nArgs) {
                    inputPathStr = args[argc++];
                } else {
                    usage("-i requires an argument.");
                }
            } else if (thisArg.equals("-d")) {
                if (argc < nArgs) {
                    delimiter = args[argc++];
                } else {
                    usage("-d requires an argument. ");
                }
            } else if (thisArg.equals("-m")) {
                if (argc < nArgs) {
                    majorArgs = args[argc++];
                } else {
                    usage("-m requires an argument");
                }
            } else if (thisArg.equals("-n")) {
                if (argc < nArgs) {
                    minorArgs = args[argc++];
                } else {
                    usage("-n requires an argument");
                }
            } else if (thisArg.equals("-v")) {
                if (argc < nArgs) {
                    valueArgs = args[argc++];
                } else {
                    usage("-v requires an argument");
                }
            } else if (thisArg.equals("-l")) {
                load = true;
            } else if (thisArg.equals("-h")) {
                if (argc < nArgs) {
                    hostName = args[argc++];
                } else {
                    usage("-h requires an argument");
                }
            } else if (thisArg.equals("-p")) {
                if (argc < nArgs) {
                    hostPort = args[argc++];
                } else {
                    usage("-p requires an argument");
                }
            } else if (thisArg.equals("-s")) {
                if (argc < nArgs) {
                    storeName = args[argc++];
                } else {
                    usage("-s requires an argument");
                }
            } else if (thisArg.equals("-t")) {
                if (argc < nArgs) {
                    fileType = args[argc++];
                } else {
                    usage("-t requires an argument");
                }
            } else if (thisArg.equals("-c")) {
                cleanKVStore = true;
            } else if (thisArg.equals("-u")) {
                usage("");
            } else if (thisArg.equals("-k")) {
                printKVStore = true;
            } else {
                usage("'" + thisArg + "' is not an expected argument.");
            }
        } //EOF while
        
        init();

    }
    
    public void init() {
    	
        if (validate()) {
            System.out.println("Validation was successful.");
            printKeys(printKVStore,cleanKVStore);
        } else {
            System.out.println("Validation failed.");
            System.exit(1);
        }
    }
    

    
    /**
     * This method performs validation on the input arguments. If this method
     * fails then program exits.
     * @return true if there is no validation errors otherwise false.
     */
    public boolean validate() {
        boolean flag = true;

        //To clean the kv-store and print the keys we don't need -m, -v, -i 
        //arguments otherwise they are required.
        if (!(printKVStore || cleanKVStore)) {
            
            if (!FileUtil.isValidPath(inputPathStr)) {
                flag = false;
                logger.logError("Input directory argument: '-i' can not be empty.");
            }

            if (StringUtil.isEmpty(majorArgs)) {
                flag = false;
                logger.logError("MajorKey argument '-m' can not be empty.");
            }

            if (StringUtil.isEmpty(valueArgs)) {
                flag = false;
                logger.logError("Value argument '-v' can not be empty.");
            }
        }

        return flag;
    }
    
    public KVStore getKVConnection() {
        if (kvStore == null) {
            kvStore =
                    KVStoreFactory.getStore(new KVStoreConfig(storeName, getHostPort()));
        }
        return kvStore;
    }
    
    public String getHostPort() {
        return hostName + ":" + hostPort;
    }

    
    protected void printKeys(boolean printKVStore, boolean cleanKVStore ) {   	
    	
    	kvStore = getKVConnection();
        if (printKVStore || cleanKVStore) {
            if (cleanKVStore)
                System.out.println("Going to delete all the keys from the store ...");

            Iterator<Key> keyIter =
                kvStore.storeKeysIterator(Direction.UNORDERED, /*batchSize*/0,
                    /*parentKey*/null, /*subRange*/null, /*depth*/null);
            Key key = null;
            List<String> list;
            int count = 0;

            while (keyIter.hasNext()) {
                key = keyIter.next();
                list = key.getFullPath();
                count++;
                //Print keys only if -k argument exist
                if (printKVStore)
                    System.out.println(count + " " + list.toString());

                //delete key if cleanKVStore=true
                if (cleanKVStore)
                    kvStore.delete(key);

            } //EOF while

            if (cleanKVStore)
                System.out.println("Deleted " + count +
                                   " keys from the store.");

            //Exit after printing
            System.exit(0);
        } //if()

    } //printKeys
    
	
	public static void main(String[] args) {
		
		LoaderFactory factory = null;
		AbstractLoader loader = null;

		LoaderTest test = new LoaderTest();
		test.parseCommandLineArgs(args);
		
		if(test.fileType.equals("String")){
			
			factory = new StringLoaderFactory();
			loader = factory.createLoader(kvStore,cleanKVStore,
					printKVStore, delimiter, minorArgs,
					majorArgs, valueArgs);
			
		}else if (test.fileType.equals("XML")){
			
			factory = new XMLLoaderFactory();
			loader = factory.createLoader(kvStore,cleanKVStore,
					printKVStore, delimiter, minorArgs,
					majorArgs, valueArgs);			
			
		}else if (test.fileType.equals("Json")){
			
			factory = new JsonLoaderFactory();
			loader = factory.createLoader(kvStore,cleanKVStore,
					printKVStore, delimiter, minorArgs,
					majorArgs, valueArgs);			
		}else{
			logger.logError("improper file");
			return;
		}		
				

		try {
			loader.loadData(inputPathStr);
		} catch (IOException e) {

			e.printStackTrace();
		}
		
		

	}

}
