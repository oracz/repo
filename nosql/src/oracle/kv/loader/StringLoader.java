package oracle.kv.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import oracle.kv.KVStore;
import oracle.kv.Key;
import oracle.kv.Value;
import oracle.kv.parser.Parser;
import oracle.kv.utils.logger;

public class StringLoader extends AbstractLoader {

	public StringLoader(KVStore kvStore,Parser parser, boolean cleanKVStore,
			boolean printKVStore, String delimiter, String minorArgs,String majorArgs, String valueArgs) {
		super(kvStore,parser, cleanKVStore, printKVStore,delimiter, minorArgs,majorArgs, valueArgs);		
		
	}
	
	@Override
	public void loadFile(File file) throws IOException {
		
        FileReader fr = null;
        Key kvKey = null;
        Value kvValue = null;
        BufferedReader br = null;

        try {
            fr = new FileReader(file);
            br = new BufferedReader(fr);
            String line = null;
            //Loop through each line and parse the content
            while ((line = br.readLine()) != null) {

            	kvKey = parser.getKey(line, delimiter, majorArgs, minorArgs);
            	kvValue = parser.getValue(line, delimiter, valueArgs);

                try {
                    //store key-value into the store
                    if (kvKey != null & kvValue != null) {
                        kvStore.put(kvKey, kvValue);
                        //count++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } //EOF while
        } catch (FileNotFoundException e) {
            logger.logError(e.getMessage());
        } catch (IOException e) {
            logger.logError(e.getMessage());
        } finally {
            if (br != null)
                br.close();
        }
		
	}

}
