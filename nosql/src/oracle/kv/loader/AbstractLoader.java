package oracle.kv.loader;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import oracle.kv.Direction;
import oracle.kv.KVStore;
import oracle.kv.KVStoreConfig;
import oracle.kv.KVStoreFactory;
import oracle.kv.Key;
import oracle.kv.parser.Parser;

public abstract class AbstractLoader {

	protected static String storeName = "kvstore";
    protected static String hostName = "localhost";
    protected static String hostPort = "5000";
    protected KVStore kvStore = null; 
    protected Parser parser;
    protected int count = 0;
    
    protected String delimiter;
    protected String minorArgs;
    protected String majorArgs;
    protected String valueArgs;
    
    protected AbstractLoader(KVStore kvStore,Parser parser, boolean cleanKVStore, boolean printKVStore, String delimiter, String minorArgs,String majorArgs, String valueArgs) {    	
    	this.parser = parser;
    	this.kvStore = kvStore;
    	this.delimiter = delimiter;
    	this.minorArgs = minorArgs;
    	this.majorArgs = majorArgs;
    	this.valueArgs = valueArgs;
    	//printKeys(cleanKVStore, printKVStore);
    } 
 
    /**
     * After successful validation this method is run to load the content of a
     * CSV/XML file into the kv-store
     * @throws IOException
     */
    public void loadData(String inputPathStr) throws IOException {
        File dir = new File(inputPathStr);
        File file = null;
        File[] files = null;
        int len = 0;

        //If input path is a directory then load data from all the files
        //that are under the directory. Make sure all the files are of same type
        //i.e. CSV or XML (mix and match is not allowed)
        if (dir.isDirectory()) {
            files = dir.listFiles();
            len = files.length;

            //loop through all the files and load the content one by one
            for (int i = 0; i < len; i++) {
                file = files[i];
                this.loadFile(file);
            } //EOF for

        } else {
            file=dir;
            this.loadFile(file);
        }

        System.out.println("Total " + count +
                           " records inserted into the KV-Store. \n" +
                           "Note: the real count of unique keys could be less.");
    } //loadData
    
   public abstract void loadFile(File file) throws IOException;
    //public abstract void loadData();

    


}
