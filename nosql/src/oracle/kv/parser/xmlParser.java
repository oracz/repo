package oracle.kv.parser;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import oracle.kv.Key;
import oracle.kv.Value;
import oracle.kv.utils.StringUtil;

public class xmlParser extends Parser {


	
    private Document m_dom = null;

  

    private String getFixedLine(String line) {
        //TODO - there is a bug in the Twitter feed i.e. retweetSourceID doesn't close with '>'
        line = line.replace("/retweetSourceID<", "/retweetSourceID><");
        line = line.replaceAll("&", "");

        return line;
    }

    private Document getXmlDocument(String line) {
        //get the factory
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // convert String into InputStream
        InputStream is = null;
        Document dom = null;

        try {

            if (StringUtil.isNotEmpty(line)) {

                line = getFixedLine(line);

                //convert line into InputStream
                is = new ByteArrayInputStream(line.getBytes());

                //Using factory get an instance of document builder
                DocumentBuilder db = dbf.newDocumentBuilder();

                //parse using builder to get DOM representation of the XML file
                dom = db.parse(is);
            }

        } catch (Exception pce) {
            System.out.println(line);            
        }

        return dom;

    } //parseXml


    private String getTagValue(Document dom, String tagName) {
        NodeList nodeList = null;

        if (dom != null && StringUtil.isNotEmpty(tagName)) {
            nodeList = dom.getElementsByTagName(tagName);
        }
        //System.out.println("NodeList: " + nodeList.getLength() + " TagName: " + tagName);
        return nodeList != null ? "" + nodeList.item(0).getTextContent() :
               null;
    }


    private List<String> getKey(String keyArgs, Document dom) {
        String token;

        List<String> keys = new ArrayList<String>();
        if (StringUtil.isNotEmpty(keyArgs) && dom != null) {

            //delimiter used for input arguments
            StringTokenizer st = new StringTokenizer(keyArgs, " ");

            while (st.hasMoreTokens()) {
                token = st.nextToken().trim();

                //If token has < or > then it is a tag therefore get the value
                //of the TagName
                token = getValue(dom, token);

                //Add the token into the list
                keys.add(token);

            } //EOF while
        } //EOF if
        //System.out.println("Keys: " + keys);
        return keys;
    } //getKey

    private boolean isTag(String token) {
        boolean flag = false;
        int first;
        int last;


        if (StringUtil.isNotEmpty(token)) {

            first = token.indexOf("<");
            last = token.indexOf(">");

            if (first == 0 && last == token.length() - 1)
                flag = true;
        }

        return flag;
    }

    private String getValue(Document dom, String token) {

        String value = null;
        if (StringUtil.isNotEmpty(token)) {
            token = token.trim();

            //if token starts with '<' && ends with '>'
            if (isTag(token)) {
                //remove '<' & '>' from the token
                token = token.substring(1, token.length() - 1);
                value = getTagValue(dom, token);
            } else {
                value = token;
            }
        }
        return value;
    }
    
 	
	@Override
	public Key getKey(String line, String delimiter, String majorArgs,
			String minorArgs) {
		
        Key key = null;
        m_dom = getXmlDocument(line);
        if (m_dom != null) {
            List<String> majorKeyComponents = getKey(majorArgs.trim(), m_dom);
            List<String> minorKeyComponents = getKey(minorArgs.trim(), m_dom);
            key = Key.createKey(majorKeyComponents, minorKeyComponents);
        }
        return key;
		
	}

	@Override
	public Value getValue(String line, String delimiter, String valueArgs) {
		
        Value value = null;
        StringBuffer sb = new StringBuffer();
        m_dom = getXmlDocument(line);
        if (m_dom != null) {
            List<String> valueComponents = getKey(valueArgs.trim(), m_dom);
            Iterator iter = valueComponents.iterator();
            while (iter.hasNext()) {
                sb.append(iter.next());
                sb.append(delimiter);
            }
            //System.out.println("Value: " + sb.toString());
            value = Value.createValue(sb.toString().getBytes());
        }
        return value;
		
		

	}
	
	
	
	
	

}
