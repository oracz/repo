package oracle.kv.parser;

import oracle.kv.Key;
import oracle.kv.Value;

public abstract class Parser {
	
   public abstract Key getKey(String line, String delimiter, String majorArgs,String minorArgs);
   public abstract Value getValue(String line, String delimiter,String valueArgs);


}
