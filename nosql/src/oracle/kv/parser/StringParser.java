package oracle.kv.parser;

import java.util.ArrayList;

import java.util.List;
import java.util.StringTokenizer;
import java.util.Iterator;



import oracle.kv.Key;
import oracle.kv.Value;
import oracle.kv.utils.StringUtil;

public class StringParser  extends Parser {
	
	
    private List<String> columns = null;
    
    public StringParser() {
        super();
    }

    private List<String> getColumns(String delimiter, String line) {

        String token;
        columns = new ArrayList<String>();    
        
        if (StringUtil.isNotEmpty(line)) {
            line = line.trim();
            StringTokenizer st = new StringTokenizer(line, delimiter);
            
            while(st.hasMoreTokens()){
                token = st.nextToken();
                columns.add(token);
            }
           
        } //EOF if
        System.out.println("Columns: " + columns);
        return columns;
    } //getColumns

    private List<String> getKey(String keyArgs, List<String> columns) {
        String token;

        int index;
        List<String> keys = new ArrayList<String>();

        int size = 0;
        if (StringUtil.isNotEmpty(keyArgs) && columns != null) {
            size = columns.size();            
            //delimiter used for input arguments
            StringTokenizer st = new StringTokenizer(keyArgs, " ");            
                     
            while (st.hasMoreTokens()) {
                token = st.nextToken().trim();
                try {
                    index = Integer.parseInt(token);
                    if (index > size) {
                        System.out.println("Error: No column at index " +
                                           index + ". Max index could be " +
                                           size + " only.");
                        System.exit(1);
                    }

                    keys.add(columns.get(index-1));

                } catch (NumberFormatException ne) {
                    //do nothing
                    keys.add(token);
                }
            } //EOF while
        } //EOF if
        System.out.println("Keys: " + keys);
        return keys;
    } //getKey



	@Override
	public Key getKey(String line, String delimiter, String majorArgs, String minorArgs) {
		
        columns=getColumns(delimiter, line);        
        List<String> majorKeyComponents = getKey(majorArgs.trim(), columns);
        List<String> minorKeyComponents = getKey(minorArgs.trim(), columns);        
        Key key = Key.createKey(majorKeyComponents, minorKeyComponents);
        
        return key;
		
		
	}

	@Override
	public Value getValue(String line, String delimiter, String valueArgs) {
		
        StringBuffer sb= new StringBuffer();
        columns=getColumns(delimiter, line);
        List <String> valueComponents = getKey(valueArgs.trim(), columns);
        Iterator<String> iter = valueComponents.iterator();
        
        while(iter.hasNext()){
            sb.append(iter.next());
            sb.append(delimiter);            
        }
        System.out.println("Value: " + sb.toString());
        Value value = Value.createValue(sb.toString().getBytes());
        
        return value;
	
	}

}
