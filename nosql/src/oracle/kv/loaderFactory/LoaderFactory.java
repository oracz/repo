package oracle.kv.loaderFactory;

import oracle.kv.KVStore;
import oracle.kv.loader.AbstractLoader;



public abstract class LoaderFactory {
	

	 public abstract AbstractLoader createLoader(KVStore kvStore, boolean cleanKVStore,boolean printKVStore, String delimiter, String minorArgs,String majorArgs, String valueArgs);
		 

}
