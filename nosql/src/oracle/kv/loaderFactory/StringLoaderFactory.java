package oracle.kv.loaderFactory;

import oracle.kv.KVStore;
import oracle.kv.loader.AbstractLoader;
import oracle.kv.loader.StringLoader;
import oracle.kv.parser.Parser;
import oracle.kv.parser.StringParser;

public class StringLoaderFactory extends LoaderFactory {
	
	public AbstractLoader createLoader(KVStore kvStore,boolean cleanKVStore,
			boolean printKVStore, String delimiter, String minorArgs,
			String majorArgs, String valueArgs) {
		StringParser sp = new StringParser();
		StringLoader StringLoader = new StringLoader(kvStore,sp,cleanKVStore,printKVStore,delimiter,minorArgs,majorArgs,valueArgs);		
		return StringLoader;
	}



}
