package oracle.kv.loaderFactory;
import oracle.kv.KVStore;
import oracle.kv.loader.AbstractLoader;
import oracle.kv.loader.XmlLoader;
import oracle.kv.parser.xmlParser;

public class XMLLoaderFactory extends LoaderFactory {
	


	@Override
	public AbstractLoader createLoader(KVStore kvStore,boolean cleanKVStore,
			boolean printKVStore, String delimiter, String minorArgs,
			String majorArgs, String valueArgs) {
		
		xmlParser xp = new xmlParser();
		XmlLoader xmlLoader = new XmlLoader(kvStore,xp,cleanKVStore,printKVStore,delimiter,minorArgs,majorArgs,valueArgs);		
		return xmlLoader;
	}

}
